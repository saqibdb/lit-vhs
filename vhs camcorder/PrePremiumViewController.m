//
//  PrePremiumViewController.m
//  vhs camcorder
//
//  Created by ibuildx Macbook 1 on 5/7/19.
//  Copyright © 2019 Shadi Osta. All rights reserved.
//

#import "PrePremiumViewController.h"
#import "PremiumPageViewController.h"
#import "PremiumPageYearlyViewController.h"
#import "Util.h"

@interface PrePremiumViewController ()

@end

@implementation PrePremiumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)monthlyAction:(UIButton *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
     NSString *vStoryboardName = [Util is4InchIPhoneDevice] ? @"Main" : @"Main";
     UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:vStoryboardName bundle:nil];
     PremiumPageViewController *premiumController = [mainStoryboard instantiateViewControllerWithIdentifier:@"premium_scene"];
     [self presentViewController:premiumController animated:YES completion:^{}];
     });
}

- (IBAction)yearlyAction:(UIButton *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *vStoryboardName = [Util is4InchIPhoneDevice] ? @"Main" : @"Main";
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:vStoryboardName bundle:nil];
        PremiumPageYearlyViewController *premiumController = [mainStoryboard instantiateViewControllerWithIdentifier:@"premium_scene_yearly"];
        [self presentViewController:premiumController animated:YES completion:^{}];
    });
}
@end
