//
//  main.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/4/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
