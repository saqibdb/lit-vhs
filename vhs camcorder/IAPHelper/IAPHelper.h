//
//  IAPHelper.h
//  In App Rage
//
//  Created by Ray Wenderlich on 9/5/12.
//  Copyright (c) 2012 Razeware LLC. All rights reserved.
//

#import <StoreKit/StoreKit.h>
#define PREMIUM_IAP @"com.vidiproject.vhseditor.premium.subscription"
#define PREMIUM_IAP_YEARLY @"com.vidiproject.vhseditor.premium.subscription.yearly"
UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;
UIKIT_EXTERN NSString *const IAPHelperProductPurchasingNotification;

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface IAPHelper : NSObject

@property(nonatomic) NSArray *productsList;

+ (IAPHelper*) shared;
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (void)buyProduct:(SKProduct *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;
- (void)restoreCompletedTransactions;
- (int) getPurchaseCompleted;
- (void) clearPurchaseComplete;
//- (NSData *)receiptForPaymentTransaction:(SKPaymentTransaction *)transaction;

-(int) getRestoreComplete;
-(void) clearRestoreComplete;
@end
