//
//  PremiumPageYearlyViewController.h
//  vhs camcorder
//
//  Created by ibuildx Macbook 1 on 5/6/19.
//  Copyright © 2019 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PremiumPageYearlyViewController : UIViewController
@property(nonatomic, retain)IBOutlet UILabel *headerLabel;
@property(nonatomic, retain)IBOutlet UILabel *importLabel;
@property(nonatomic, retain)IBOutlet UILabel *montageLabel;
@property(nonatomic, retain)IBOutlet UILabel *musicLabel;
@property(nonatomic, retain)IBOutlet UILabel *videoLabel;
@property(nonatomic, retain)IBOutlet UILabel *adLabel;
@property(nonatomic, retain)IBOutlet UILabel *watermarkLabel;

@property(nonatomic, retain)IBOutlet UILabel *premiumLabel;
@property(nonatomic, retain)IBOutlet UIButton *restoreButton;
@property(nonatomic, retain)IBOutlet UIButton *premiumButton;

@property (weak, nonatomic) IBOutlet UIButton *legalBTn;



-(IBAction) goBack;
-(IBAction) goPremium;
-(IBAction) restorePurchases;
- (IBAction)legatAction:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
