//
//  constants.h
//  Our Pills Talk
//
//  Created by Ajaz on 12/3/14.
//  Copyright (c) 2014 Aim. All rights reserved.
//

#define PINKCOLOR [UIColor colorWithRed: 208.0f/255.0f green: 85.0f/255.0f blue: 163.0f/255.0f alpha: 1]
#define IS_IPAD     (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define BebasNeueFONT     @"BebasNeue"
#define Spacing     @1.0
