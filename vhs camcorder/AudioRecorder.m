//
//  AudioRecorder.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 4/27/17.
//  Copyright © 2017 Shadi Osta. All rights reserved.
//

#import "AudioRecorder.h"

@implementation AudioRecorder

- (instancetype)init
{
    self = [super init];
    if (self) {
        audioOutputFileURL = [NSURL fileURLWithPath:[self generateAudioOutputFilePath]];
        
        // Setup audio session
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        /*[session setCategory:AVAudioSessionCategoryPlayAndRecord
                 withOptions:AVAudioSessionCategoryOptionMixWithOthers|AVAudioSessionCategoryOptionDefaultToSpeaker
                       error:nil];*/
        
        // Define the recorder setting
        NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
        
        [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
        [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
        
        // Initiate and prepare the recorder
        recorder = [[AVAudioRecorder alloc] initWithURL:[self getAudioOutputFileURL] settings:recordSetting error:NULL];
        recorder.delegate = self;
        //recorder.meteringEnabled = YES;
        [recorder prepareToRecord];

    }
    return self;
}

- (void)beginRecording {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    
    // Start recording
    [recorder record];
}

- (void)endRecording {
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (BOOL)isAudioRecording {
    return recorder.recording;
}

- (void)resetAudioRecordSession {
}

- (NSURL*)getAudioOutputFileURL {
    return audioOutputFileURL;
}

- (NSString*)generateAudioOutputFilePath {
    NSString *tempDirectory = NSTemporaryDirectory();
    NSString *fileLastName  = [NSString stringWithFormat:@"videoAudioRecording-%d.m4a", arc4random() % 1000000];
    NSString *myPathDocs    = [tempDirectory stringByAppendingPathComponent:fileLastName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    
    return myPathDocs;
}

#pragma mark - AVAudioRecorderDelegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag {
}

@end
