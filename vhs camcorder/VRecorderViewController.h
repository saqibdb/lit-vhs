//
//  ViewController.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/4/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCRecorder.h"
#import "GPUImage.h"
#import <AVKit/AVKit.h>
#import "RGBChannelCompositing.h"
//#import <Google/Analytics.h>
#import <MediaPlayer/MediaPlayer.h>
#import "OneFingerRotationGestureRecognizer.h"
@interface VRecorderViewController : UIViewController <SCRecorderDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, SCAssetExportSessionDelegate, AVPlayerViewControllerDelegate, MPMediaPickerControllerDelegate,UIScrollViewDelegate, OneFingerRotationGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *circleView;
@property (weak, nonatomic) IBOutlet UIImageView *camBorders;
@property (weak, nonatomic) IBOutlet UIImageView *sidesAnimView;
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UILabel *timeRecordedLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *playLabel;
@property (weak, nonatomic) IBOutlet UIButton *recordBtn;
@property (weak, nonatomic) IBOutlet UILabel *watermarkLabel;
@property (weak, nonatomic) IBOutlet UIButton *effectButton;
@property (weak, nonatomic) IBOutlet UIButton *recBTN;
@property (weak, nonatomic) IBOutlet UIButton *playBTN;
@property (weak, nonatomic) IBOutlet UIButton *playTriangleBTN;
@property (weak, nonatomic) IBOutlet UIImageView *imageToMove;

@property (weak, nonatomic) IBOutlet UILabel *contrastPercentage;
//@property (strong, nonatomic) SCAssetExportSession *exportSession;

@property (strong, nonatomic) GPUImageMovie *movieFile;
@property (strong, nonatomic) GPUImageMovie *secondmovieFile;
@property (strong, nonatomic) GPUImageMovieWriter *movieWriter;
@property (strong, nonatomic) GPUImageFilter *filter;
@property (strong, nonatomic) GPUImageAlphaBlendFilter *blendFilter;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) GPUImageUIElement *uiElementInput;

@property (weak, nonatomic) IBOutlet UISlider *zoomSlider;
@property (weak, nonatomic) IBOutlet UISlider *wffwctsSlider;










-(IBAction) openSettings;
-(IBAction) startRecording;
-(IBAction) openCameraRoll;
-(IBAction)reverseCamera:(id)sender;
-(IBAction)switchFlash:(id)sender;
-(IBAction)musicAction:(id)sender;
-(IBAction)downloadAction:(id)sender;
-(IBAction)photoAction:(id)sender;

- (IBAction)zoomChanged:(UISlider *)sender;

- (IBAction)effectsSliderChanged:(UISlider *)sender;

- (IBAction)importAxtion:(UIButton *)sender;




@end

