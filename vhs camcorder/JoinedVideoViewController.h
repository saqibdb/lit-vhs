//
//  JoinedVideoViewController.h
//  VidEditor
//
//  Created by ibuildx on 11/1/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>

@interface JoinedVideoViewController : UIViewController <AVPlayerViewControllerDelegate>
- (IBAction)backAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIView *outerPlayerVIEW;
@property (nonatomic) AVPlayerViewController *playerViewController;

- (IBAction)saveAction:(UIButton *)sender;
- (IBAction)instagramAction:(UIButton *)sender;
- (IBAction)facebookAction:(UIButton *)sender;
- (IBAction)rateAction:(UIButton *)sender;
@property (nonatomic, strong) NSURL *selectedURL;
@property BOOL isSaveClicked ;
@property BOOL isWithAudio ;
@end
