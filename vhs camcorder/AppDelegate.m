// com.shadiosta.vhsEditor
//  AppDelegate.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/4/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//
//com.shadiosta.vhsEditor
//com.vidiproject.LIT-VHS
#import "AppDelegate.h"
#import "ALSdk.h"
#import "IAPHelper.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Util.h"
#import "Util.h"

//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    [Fabric with:@[[Crashlytics class]]];

//    UIFont *headerFont;
//    headerFont = [UIFont fontWithName:@"" size:60.0];
    
    UIStoryboard *storyboard = [Util grabStoryboard];
    // show the storyboard
    self.window.rootViewController = [storyboard instantiateInitialViewController];
    
    // Configure tracker from GoogleService-Info.plist.
    /*NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release*/
    
//    [NSThread sleepForTimeInterval:1.0];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //_interstitial = [self createAndLoadInterstitial];
    
    BOOL firstRun = ![[NSUserDefaults standardUserDefaults] boolForKey:@"is_first_run"];
    if (firstRun) [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"noof_app_run"];
    
    [self initialize];
    //[self getBasicFBPermission];
    [self getSavePermissions];
    
//    [self.window makeKeyAndVisible];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    [self checkTheReceiptStatus];

    
    return YES;
}

- (void)getSavePermissions {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined)
        {
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status)
            {
                if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized)
                {
                    NSLog(@"PHAuthorizationStatusAuthorized");
                }
            }];
        }
    });
}

-(void)initialize {
    [Util createMyVideosFolderInDocuments];
//    [ALSdk initializeSdk];
    [IAPHelper shared];
    
    NSUserDefaults* userDefaults  = [NSUserDefaults standardUserDefaults];
    /*NSDateFormatter *dateformater = [[NSDateFormatter alloc] init];
    [dateformater setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateformater setDateFormat:@"MMM.dd yyyy"];
    
    NSString *date = [dateformater stringFromDate: [NSDate date]];
    [userDefaults setObject:[NSString stringWithFormat:@"%@", date] forKey:@"fake_date_created"];*/

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"date_clicked_to_rate"])
        [Util handleRatePopup];
        
    [userDefaults setObject:@"ADD TITLES" forKey:@"text_1"];
    [userDefaults setObject:@"TAP ON SETTINGS TO EDIT" forKey:@"text_2"];
    [userDefaults setObject:@"white" forKey:@"text_color"];
    [userDefaults synchronize];
    
    [Util downloadAndCheckIFRateWindowEnable];
}

- (GADInterstitial *)createAndLoadInterstitial {//
    //GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-8900215750179558/9054019027"];
    
    //
    
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-1634700826148036/8172038601"];
    
    
    
    //GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-3940256099942544/4411468910"]; TEST GOOGLE AD
    
    
    interstitial.delegate = self;
    GADRequest * request = [GADRequest request];
    request.testDevices = @[ @"e3ff539ab1cc6a9bdc38d4d541743120" ];
    [interstitial loadRequest:request];
    return interstitial;
}


- (void)getBasicFBPermission {
    ACAccountStore* accountStore = [[ACAccountStore alloc] init];
    NSDictionary *options = @{
                              ACFacebookAppIdKey: @"1291941714279751",
                              ACFacebookPermissionsKey: @[@"email", ],
                              ACFacebookAudienceKey: ACFacebookAudienceOnlyMe
                              };
    ACAccountType *facebookAccountType = [accountStore
                                          accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    [accountStore requestAccessToAccountsWithType:facebookAccountType options:options completion:^(BOOL granted, NSError *error) {
        if (granted) {
            NSLog(@"access granted");
        } else {
            NSLog(@"access to facebook is not granted");
        }
    }];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSLog(@"applicationDidEnterBackground");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [Util handleRatePopup];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    NSInteger runTimes = [[NSUserDefaults standardUserDefaults] integerForKey:@"noof_app_run"];
    runTimes++;
    [[NSUserDefaults standardUserDefaults] setInteger:runTimes forKey:@"noof_app_run"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"applicationWillTerminate");
}


#pragma mark - GADInterstitialDelegate

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}



/// Tells the delegate an ad request succeeded.
- (void)interstitialDidReceiveAd:(DFPInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    if (![Util isPremiumUser]) {
        if ([Util isFullAccessForInitialDays]) {
            NSLog(@"In first 3 days_____________");
            return;
        }
        int adValue = [Util adPopValue];
        if (adValue == 0) {
            [ad presentFromRootViewController:[self topViewController]];
        }
        else{
            NSLog(@"Advert Pop Value = %i" , adValue);
        }
    }
    else{
        NSLog(@"Premium User");
    }
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(DFPInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that an interstitial will be presented.
- (void)interstitialWillPresentScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

/// Tells the delegate the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

/// Tells the delegate that a user click will open another app
/// (such as the App Store), backgrounding the current app.
- (void)interstitialWillLeaveApplication:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}


//#pragma mark - GADInterstitialDelegate
//
//- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
//    self.interstitial = [self createAndLoadInterstitial];
//}






- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}


+ (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}


- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions NS_AVAILABLE(10_7, 3_0){
    NSLog(@"updatedTransactions = %@" , transactions);
    
    for(SKPaymentTransaction *transaction in transactions){
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                [self handlePurchasingStateForTransaction:transaction inQueue:queue];
                break;
            case SKPaymentTransactionStatePurchased:
                [self handlePurchasedStateForTransaction:transaction inQueue:queue];
                break;
            case SKPaymentTransactionStateFailed:
                [self handleFailedStateForTransaction:transaction inQueue:queue];
                break;
            case SKPaymentTransactionStateRestored:
                [self handleRestoredStateForTransaction:transaction inQueue:queue];
                break;
            case SKPaymentTransactionStateDeferred:
                [self handleDeferredStateForTransaction:transaction inQueue:queue];
                break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray<SKPaymentTransaction *> *)transactions NS_AVAILABLE(10_7, 3_0){
    NSLog(@"removedTransactions = %@" , transactions);

}

// Sent when an error is encountered while adding transactions from the user's purchase history back to the queue.
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error NS_AVAILABLE(10_7, 3_0){
    NSLog(@"restoreCompletedTransactionsFailedWithError = %@" , error);
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"off_watermark"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"vhs_is_premium_purchased"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// Sent when all transactions from the user's purchase history have successfully been added back to the queue.
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue NS_AVAILABLE(10_7, 3_0){
    NSLog(@"paymentQueueRestoreCompletedTransactionsFinished = %@" , queue);

    [self checkTheReceiptStatus];


}

// Sent when the download state has changed.
- (void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray<SKDownload *> *)downloads NS_AVAILABLE(10_8, 6_0){
    NSLog(@"updatedDownloads = %@" , downloads);

}



-(void)handlePurchasingStateForTransaction:(SKPaymentTransaction *)transaction inQueue:(SKPaymentQueue*)queue {
    NSLog(@"User is attempting to purchase product id: %@", transaction.payment.productIdentifier);
}



-(void)handlePurchasedStateForTransaction:(SKPaymentTransaction *)transaction inQueue:(SKPaymentQueue*)queue {
    NSLog(@"User purchased product id: %@", transaction.payment.productIdentifier);
    
    
    [queue finishTransaction:transaction];
    [self checkTheReceiptStatus];
    
    
}
-(void)handleRestoredStateForTransaction:(SKPaymentTransaction *)transaction inQueue:(SKPaymentQueue*)queue {
    NSLog(@"Purchase restored for product id: %@", transaction.payment.productIdentifier);
}
-(void)handleFailedStateForTransaction:(SKPaymentTransaction *)transaction inQueue:(SKPaymentQueue*)queue {
    NSLog(@"Purchase failed for product id: %@", transaction.payment.productIdentifier);
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"off_watermark"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"vhs_is_premium_purchased"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
-(void)handleDeferredStateForTransaction:(SKPaymentTransaction *)transaction inQueue:(SKPaymentQueue*)queue {
    NSLog(@"Purchase deferred for product id: %@", transaction.payment.productIdentifier);
}


-(NSData *)loadReceipt {
    NSURL* url = [[NSBundle mainBundle] appStoreReceiptURL];
    if (!url) {
        return nil;
    }
    NSData *data = [NSData dataWithContentsOfURL:url];
    return data;
}


-(void)checkTheReceiptStatus {
    NSData* receiptData = [self loadReceipt];
    if (receiptData) {
        NSLog(@"TEHRE IS DATA");
        [Util verifyReceiptWithData:receiptData AndResponseBlock:^(id object, BOOL status, NSError *error) {
            if(status){
                NSDictionary *dictLatestReceiptsInfo = object[@"latest_receipt_info"];
                long long int expirationDateMs = [[dictLatestReceiptsInfo valueForKeyPath:@"@max.expires_date_ms"] longLongValue];
                long long requestDateMs = [object[@"receipt"][@"request_date_ms"] longLongValue];
                NSLog(@"%lld--%lld", expirationDateMs, requestDateMs);
                BOOL rs = [[object objectForKey:@"status"] integerValue] == 0 && (expirationDateMs > requestDateMs);
                if(rs){
                    NSLog(@"NOT EXPIRED");
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"off_watermark"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"vhs_is_premium_purchased"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else{
                    NSLog(@"EXPIRED");
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"off_watermark"];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"vhs_is_premium_purchased"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
            }
            else{
                NSLog(@"ERROR = %@" , error.description);
            }
        }];
    }
    else{
        NSLog(@"TEHRE IS NO DATA");
    }
}


@end
