//
//  PrePremiumViewController.h
//  vhs camcorder
//
//  Created by ibuildx Macbook 1 on 5/7/19.
//  Copyright © 2019 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PrePremiumViewController : UIViewController





- (IBAction)backAction:(UIButton *)sender;

- (IBAction)monthlyAction:(UIButton *)sender;
- (IBAction)yearlyAction:(UIButton *)sender;


@end

NS_ASSUME_NONNULL_END
