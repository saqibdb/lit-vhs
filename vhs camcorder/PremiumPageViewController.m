//
//  PremiumPageViewController.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/11/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "PremiumPageViewController.h"
#import "IAPHelper.h"
#import "MBProgressHUD.h"
#import "Util.h"
#import "constants.h"
#import "LegalViewController.h"

@interface PremiumPageViewController ()

@end

@implementation PremiumPageViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{

        UIFont *font;
        if(IS_IPAD)
        {
            font = [UIFont fontWithName:BebasNeueFONT size:36.0];
        }
        else
        {
            font = [UIFont fontWithName:BebasNeueFONT size:22.0];
        }
        
        NSMutableDictionary *Aa = [[NSMutableDictionary alloc] init];
        [Aa setObject:font forKey:NSFontAttributeName];
        [Aa setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        [Aa setObject:Spacing forKey:NSKernAttributeName];
        NSAttributedString *importAttrString = [[NSAttributedString alloc] initWithString:@"Import Photos and Videos" attributes:Aa];
        self.importLabel.attributedText = importAttrString;
        
        NSAttributedString *montageAttrString = [[NSAttributedString alloc] initWithString:@"Create Montages" attributes:Aa];
        self.montageLabel.attributedText = montageAttrString;
        
        NSAttributedString *musicAttrString = [[NSAttributedString alloc] initWithString:@"Add Music" attributes:Aa];
        self.musicLabel.attributedText = musicAttrString;
        
        NSAttributedString *videoAttrString = [[NSAttributedString alloc] initWithString:@"Longer Videos" attributes:Aa];
        self.videoLabel.attributedText = videoAttrString;
        
        NSAttributedString *adAttrString = [[NSAttributedString alloc] initWithString:@"No More Ads" attributes:Aa];
        self.adLabel.attributedText = adAttrString;
        
        NSAttributedString *watermarAttrString = [[NSAttributedString alloc] initWithString:@"No More Watermark" attributes:Aa];
        self.watermarkLabel.attributedText = watermarAttrString;
        
        UIFont *headerFont;
        
        if(IS_IPAD)
        {
            headerFont = [UIFont fontWithName:BebasNeueFONT size:60.0];
        }
        else
        {
            headerFont = [UIFont fontWithName:BebasNeueFONT size:30.0];
        }
        
        NSMutableDictionary *headerDict = [[NSMutableDictionary alloc] init];
        [headerDict setObject:headerFont forKey:NSFontAttributeName];
        [headerDict setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        [headerDict setObject:Spacing forKey:NSKernAttributeName];
        NSAttributedString *headerAttrString = [[NSAttributedString alloc] initWithString:@"Go Premium" attributes:headerDict];
        self.headerLabel.attributedText = headerAttrString;
        
        UIFont *premiumFont;
        
        premiumFont = [UIFont fontWithName:BebasNeueFONT size:24.0];
        
        NSMutableDictionary *premiumDict = [[NSMutableDictionary alloc] init];
        [premiumDict setObject:premiumFont forKey:NSFontAttributeName];
        [premiumDict setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        [premiumDict setObject:Spacing forKey:NSKernAttributeName];
        NSAttributedString *premiumAttrString = [[NSAttributedString alloc] initWithString:@"Premium Version is only $1.99 for a limited time!" attributes:premiumDict];
        self.premiumLabel.attributedText = premiumAttrString;
        /*
        UIFont *purchaseBtnFont = [UIFont fontWithName:BebasNeueFONT size:24.0];
        
        NSMutableDictionary *butn1Dict = [[NSMutableDictionary alloc] init];
        [butn1Dict setObject:purchaseBtnFont forKey:NSFontAttributeName];
        [butn1Dict setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        [butn1Dict setObject:Spacing forKey:NSKernAttributeName];
        NSAttributedString *btn1AttrString = [[NSAttributedString alloc] initWithString:@"Go Premium" attributes:butn1Dict];
        
        [self.premiumButton setAttributedTitle:btn1AttrString forState:UIControlStateNormal];*/
//        [self.premiumButton.layer setCornerRadius:self.premiumButton.frame.size.height/2.0];
//        self.premiumButton.layer.masksToBounds = YES;
        
        UIFont *restoreFont;
        
        if(IS_IPAD)
        {
            restoreFont = [UIFont fontWithName:BebasNeueFONT size:30.0];
        }
        else
        {
            restoreFont = [UIFont fontWithName:BebasNeueFONT size:19.0];
        }
        
        NSMutableDictionary *restoreDict = [[NSMutableDictionary alloc] init];
        [restoreDict setObject:restoreFont forKey:NSFontAttributeName];
        [restoreDict setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        [restoreDict setObject:Spacing forKey:NSKernAttributeName];
        NSAttributedString *restoreAttrString = [[NSAttributedString alloc] initWithString:@"Restore Purchase" attributes:restoreDict];
        //[self.restoreButton setAttributedTitle:restoreAttrString forState:UIControlStateNormal];
        
        UIFont* legalBTnFont = [UIFont fontWithName:BebasNeueFONT size:15.0];

        [restoreDict setObject:legalBTnFont forKey:NSFontAttributeName];
        NSAttributedString *legalAttrString = [[NSAttributedString alloc] initWithString:@"Legal Informations" attributes:restoreDict];

        [self.legalBTn setAttributedTitle:legalAttrString forState:UIControlStateNormal];

    });

    
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
    
}

#pragma mark - Navigation

- (IBAction)goBack {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)goPremium {
    if (![Util isInternetReachable]) {
        [self showInternetAlert];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(purchaseTransaction:)
                                                 name:IAPHelperProductPurchasingNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(purchaseTransaction:)
                                                 name:IAPHelperProductPurchasedNotification
                                               object:nil];

    if([[[IAPHelper shared] productsList] count] > 0){
        for(SKProduct *product in [[IAPHelper shared] productsList]){
            if ([product.productIdentifier isEqualToString:PREMIUM_IAP]) {
                [[IAPHelper shared] buyProduct:product];
                break;
            }
        }
    }
    else{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
}

- (IBAction)restorePurchases {
    if (![Util isInternetReachable]) {
        [self showInternetAlert];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"Restoring Transactions";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(restoredTransactions:)
                                                 name:IAPHelperProductPurchasedNotification
                                               object:nil];

    [[IAPHelper shared] restoreCompletedTransactions];
}

- (IBAction)legatAction:(UIButton *)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Legal Information" message:@"Please select what you need to see." preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

        // Cancel button tappped.
//        [self dismissViewControllerAnimated:YES completion:^{
//        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Privacy Policy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // Distructive button tapped.
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *vStoryboardName = [Util is4InchIPhoneDevice] ? @"Main" : @"Main";
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:vStoryboardName bundle:nil];
            LegalViewController *premiumController = [mainStoryboard instantiateViewControllerWithIdentifier:@"legal_scene"];
            [self presentViewController:premiumController animated:YES completion:^{
                [premiumController.termsTextView setHidden:YES];
                [premiumController.privacyPolicyTextView setHidden:NO];
            }];
        });
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Terms Of Use" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *vStoryboardName = [Util is4InchIPhoneDevice] ? @"Main" : @"Main";
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:vStoryboardName bundle:nil];
            LegalViewController *premiumController = [mainStoryboard instantiateViewControllerWithIdentifier:@"legal_scene"];
            [self presentViewController:premiumController animated:YES completion:^{
                [premiumController.privacyPolicyTextView setHidden:YES];
                [premiumController.termsTextView setHidden:NO];

            }];
        });
        // OK button tapped.
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)purchaseTransaction:(NSNotification*)notification {
    if ([notification.name isEqualToString:IAPHelperProductPurchasingNotification]) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"Purchasing";
        
    } else if ([notification.name isEqualToString:IAPHelperProductPurchasedNotification]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchasingNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchasedNotification object:nil];
        
        NSDictionary* userInfo = notification.userInfo;
        BOOL success = [userInfo[@"status"] boolValue];
        if (!success) {
            NSString *title   = @"Transaction Failure";
            NSString *message = userInfo[@"message"];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:title
                                                  message:message
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                              handler:^(UIAlertAction *action) {
                                                                  [self goBack];
                                                              }]];
            [self presentViewController:alertController animated:YES completion:nil];
            
        } else [self goBack];
    } else [self goBack];
}

- (void)restoredTransactions:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchasedNotification object:nil];
    
    if ([notification.name isEqualToString:IAPHelperProductPurchasedNotification]) {
        NSString *title = @"Restore";
        NSString *message = @"All Transactions have been restored!";
        NSDictionary* userInfo = notification.userInfo;
        BOOL success = [userInfo[@"status"] boolValue];
        if (!success) {
            title   = @"Restore Failure";
            message = userInfo[@"message"];
        }
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                              [self goBack];
                                                          }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else [self goBack];
}

- (void)showInternetAlert {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Network Unavailable!"
                                          message:@"Please check your internet connection."
                                          preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {

                                                      }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
