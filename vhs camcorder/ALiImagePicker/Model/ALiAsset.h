//
//  ALiAsset.h
//  ALiImagePicker
//
//  Created by ibuildx-Mac on 2016/10/15.
//  Copyright © 2016年 Saqibdb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>

@interface ALiAsset : NSObject

@property (nonatomic, strong) PHAsset *asset;

@end
