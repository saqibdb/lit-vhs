//
//  ALiImageBrowserTopToolBar.h
//  ALiImagePicker
//
//  Created by ibuildx-Mac on 2016/10/18.
//  Copyright © 2016年 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALiImageBrowserTopToolBar : UIView

@property (nonatomic, strong) UIButton *backBtn;

@property (nonatomic, strong) UIButton *selectBtn;

@end
