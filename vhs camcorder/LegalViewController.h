//
//  LegalViewController.h
//  vhs camcorder
//
//  Created by ibuildx Macbook 1 on 5/7/19.
//  Copyright © 2019 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LegalViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *privacyPolicyTextView;
@property (weak, nonatomic) IBOutlet UITextView *termsTextView;



- (IBAction)backAction:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
