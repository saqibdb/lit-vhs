//
//  SettingsViewController.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/9/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomUILabel;
@interface SettingsViewController : UIViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *removeAdsBTN;
@property (weak, nonatomic) IBOutlet UIButton *dYesBox;
//@property (weak, nonatomic) IBOutlet UIButton *dNoBox;
@property (weak, nonatomic) IBOutlet UIButton *wYesBox;
//@property (weak, nonatomic) IBOutlet UIButton *wNoBox;
@property (weak, nonatomic) IBOutlet UIButton *dateBTN;
@property (weak, nonatomic) IBOutlet CustomUILabel *dateLabel;

@property (weak, nonatomic) IBOutlet CustomUILabel *watermarkLabel;
@property (weak, nonatomic) IBOutlet UIButton *ChangeRetroTitleBTN;
@property (weak, nonatomic) IBOutlet UIButton *RateUsOnAppStoreBTN;
@property (weak, nonatomic) IBOutlet UIButton *GoPremiusmBTN;

@property (weak, nonatomic) IBOutlet UILabel *importLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (nonatomic, retain) IBOutlet CustomUILabel *screenLabel;

-(IBAction) checkBoxAction: (id)sender;
-(IBAction) dateAction: (id)sender;
-(IBAction) removeAdsAction: (id)sender;
-(IBAction) rateAction: (id)sender;

@end
