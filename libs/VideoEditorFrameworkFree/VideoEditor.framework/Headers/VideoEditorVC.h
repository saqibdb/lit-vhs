
#define VEFilter        @"VEFilter"
#define VEAdjust        @"VEAdjust"
#define VECrop          @"VECrop"
#define VEMusic         @"VEMusic"
#define VETrim          @"VETrim"
#define VEText          @"VEText"
#define VEEnhance       @"VEEnhance"
#define VEOrientation   @"VEOrientation"
#define VEBorder        @"VEBorder"
#define VESticker       @"VESticker"
#define VEFrames        @"VEFrames"
#define VESpeed         @"VESpeed"

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol VideoEditorSDKDelegate <NSObject>
@optional
-(void)videoEditorSDKFinishedWithUrl:(NSURL *)url;
-(void)videoEditorSDKCanceled;
@end

@interface VideoEditorVC : UIViewController
@property (nonatomic, weak) id<VideoEditorSDKDelegate> delegate;
@property (nonatomic,strong) NSURL *videoPath;
@property (nonatomic,strong) NSString *clientKey;
@property (nonatomic,strong) NSString *clientSecretKey;
@property (nonatomic,strong) NSArray *excludedFunctionalities;
@end
