//
//  RGBChannelCompositing.h
//  SCRecorder
//
//  Created by Qaiser Butt on 5/13/16.
//  Copyright © 2016 rFlex. All rights reserved.
//

#import <CoreImage/CoreImage.h>

@interface RGBChannelCompositing : CIFilter
{
    const CIColorKernel *rgbChannelCompositingKernel;
}

@property(nonatomic) CIImage *inputRedImage;
@property(nonatomic) CIImage *inputGreenImage;
@property(nonatomic) CIImage *inputBlueImage;

@end


@interface RGBChannelToneCurve : CIFilter
{    
    const RGBChannelCompositing *rgbChannelCompositing;
    const CIFilter              *bumpDistortion;
}

@property(nonatomic) CIImage *inputImage;
@property(nonatomic) CIVector *inputRedValuesY;
@property(nonatomic) CIVector *inputGreenValuesY;
@property(nonatomic) CIVector *inputBlueValuesY;
@property(nonatomic) NSString *currentFXName;

-(void) setDefaults;

@end

@interface VHSTrackingLines : CIFilter
{    
    CGFloat inputTime;
    CGFloat inputSpacing;
    CGFloat inputStripeHeight;
    CGFloat inputBackgroundNoise;

    int     moveCounter;
}

@property(nonatomic) CIImage *inputImage;

-(void) setDefaults;

@end
